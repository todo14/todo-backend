const express = require('express');
const app = express();
const config = require('./utils/config');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const loginController = require('./controllers/login');
const taskController = require('./controllers/task');
const userController = require('./controllers/user');
const shoppinglistItemController = require('./controllers/shoppinglistItem');
const shoppingItemBatchController = require('./controllers/shoppingItemBatch');
const storeController = require('./controllers/store');

mongoose.connect(config.MONGODB_URI)
  .then( () => {
    console.log('Connected to database');
  })
  .catch( error => {
    console.log(error);
  })

mongoose.Promise = global.Promise;

app.use(cors());
app.use(express.static('build'));
app.use(bodyParser.json());

app.use('/api/user', userController);
app.use('/api/task', taskController);
app.use('/api/login', loginController);
app.use('/api/shoppinglist', shoppinglistItemController);
app.use('/api/shoppingbatch', shoppingItemBatchController);
app.use('/api/store', storeController);

module.exports = app;