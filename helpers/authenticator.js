const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

/**
 * Compares a password to its hashed counterpart to verify similarity.
 * Uses bcrypt to do this.
 * 
 * @param {String} plainPword The plaintext version of the password
 * @param {String} hash The hashed version of the password
 */
const authenticate = async (plainPword, hash) => {
  return await bcrypt.compare(plainPword, hash);
};

/**
 * Takes in an HTTP request and extracts the token from the authorization header
 * 
 * @param {*} request 
 * @returns {String} The token in the request headers
 */
const getTokenFrom = (request) => {
  const authorization = request.get('authorization')
  if (authorization && authorization.toLowerCase().startsWith('bearer ')) {
    return authorization.substring(7)
  }
  return null
}

/**
 * Takes a bearer jwt token and decodes it, giving the contents.
 * 
 * @param {*} token
 * @returns {Object} The decoded token contents
 */
const checkToken = (token) => {
  if(token === null) {
    return false;
  }
  let decodedToken
  try {
    decodedToken = jwt.verify(token, process.env.SECRET);
  } catch (error) {
    console.log('JWT token invalid: ', error.message);
  } 
  if(!decodedToken) {
    return false;
  }
  return decodedToken;
};

module.exports = {
  authenticate,
  getTokenFrom,
  checkToken
};