# todo-backend

Backend for the todo project. Written in JavaScript.

Deployed to Heroku.

Uses MongoDB offered by MongoDB Atlas.

`MONGODB_URI` needs to be defined as the url for DB connection. It should contain
needed credentials for database access.
For dev environment, just install a local mongodb, no credentials, and use the connection string `mongodb://127.0.0.1` to connect.

`SECRET` variable needs to be defined for login / registration to work.

`NODE_ENV` should be set as `production` when deployed so that the application will pick the correct URLs.

`PORT` sets the port the application will listen to (Heroku will set this automatically).

All of the above env variables should be set with Heroku's own tools if deployed there.


### Deployment

The app deploys to Heroku.
`git push heroku master`

Running the above command will upload the backend along with the `build` folder containing the frontend, to Heroku.

Remember to build the frontend in to the `build` folder for new frontend changes


### Development

`npm run start` starts the app in the port defined as `PORT`

`npm run watch` starts the app with nodemon so changes will be applied on save

See https://docs.microsoft.com/en-us/windows/wsl/tutorials/wsl-database for instructions for running MongoDB on WSL

You will want to create a local `.env` file to contain the required environment variables listed above. Only `MONGODB_URI` and `SECRET` are required to start a working development server. For unit testing, a `TEST_MONGODB_URI` variable should be given.

On Linux, you might need to start the MongoDB with `systemctl start mongodb.service` before running the app.


#### Tests

For unit testing, a `TEST_MONGODB_URI` variable should be given.

`npm run test` to run Jest integration tests

`npm run test-watch` runs Jest reacting on filesaves

`npm run test-coverage` runs Jest tests and prints coverage in console


### Documentation

[API](https://gitlab.com/mikkot/todo-backend/blob/master/documentation/API.md)
