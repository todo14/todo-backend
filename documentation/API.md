# API

### ShoppinglistItem

Model:

```json
{
  name: String,
  done: Boolean,
  store: String,
  user: String
}
```

`name` will be the visual representation that the user sees of the item<br>
`done` does not currently affect the appearance or usability of the item in practice<br>
`store` makes no difference at the moment<br>
`user` this is the user's database id. The tasks are fetched by the logged users id<br>


### Task

Model:

```json
{
  priority: Number,
  label: String,
  description: String,
  done: Boolean,
  user: String,
  created: { type: Date, default: Date.now }
}
```

`priority` does not affect the application for now<br>
`label` name of the task visible to the user<br>
`description` a longer more accurate description that can be given for the task<br>
`done` self explanatory<br>
`user` user's database id. Used to fetch logged in users tasks<br>
`created` automatically filled by server. A time stamp for creation.<br>


### TasksInfo

Model:

```json
{
  avgTime: Number,
  amountDone: Number,
  totalTime: Number
}
```

Not implemented yet. Supposed to give the user somekind of info about their tasks


### User

Model:

```json
{
  username: {
    type: String,
    unique: true,
    require: true
  },
  passwordHash: String
}
```

`passwordHash` is generated in the controllers. User input is plaintext.