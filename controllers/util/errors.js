const databaseError = (res) => {
  return res.status(500).send('Error communicating with database');
};

export { databaseError };