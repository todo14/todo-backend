const WebSocket = require('ws');

const webSocketServer = new WebSocket.Server({ port: 3031 });

webSocketServer.on('connection', (ws, req, client) => {
  ws.on('message', (message) => {
    console.log('Received a message: ', message);
    webSocketServer.clients.forEach(client => {
      if (client.readyState === WebSocket.OPEN) {
        client.send('Responding to client');
      }
    });
  });
});

// This might not work, since there is no guarantee that the WebSocket is actually
// properly set up when this is called. If all goes well, it should be though, since
// it is supposed to be set up when the app starts. Should be fixed though.
const broadCastDataToAllClients = (data) => {
  console.log('Broadcasting data...', data);
  webSocketServer.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  })
}

module.exports = {
  webSocketServer,
  broadCastDataToAllClients,
};