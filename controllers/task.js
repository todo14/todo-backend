const taskController = require('express').Router();
const jwt = require('jsonwebtoken');
const Task = require('../models/task');
const { getTokenFrom } = require('../helpers/authenticator')

const checkToken = (token) => {
  if(token === null) {
    return false;
  }
  let decodedToken
  try {
    decodedToken = jwt.verify(token, process.env.SECRET);
  } catch (error) {
    console.log('JWT token invalid: ', error.message);
  } 
  if(!decodedToken) {
    return false;
  }
  return decodedToken;
}

taskController.post('/', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) {
    check = checkToken(token);
  } else {
    return res.status(401).send('Invalid or missing token')
  }
  if(!check) {
    return res.status(401).send('No access');
  }

  if(req.body && req.body.task) {
    const task = req.body.task;
    const newTask = new Task({
      priority: task.priority,
      label: task.label,
      description: task.description,
      done: false,
      user: task.user
    });

    let savedTask
    try {
      savedTask = await newTask.save();
    } catch(error) {
      return res.status(500).send('Error communicating with the database');
    }

    return res.status(200).json(Task.format(savedTask));

  } else {
    return res.status(400).send();
  }
});

taskController.get('/', async (req, res) => {
  let token = getTokenFrom(req);
  let check = checkToken(token);
  if(!check) {
    return res.status(401).send('No access');
  }

  let tasks
  if(check.id) {
    tasks = await Task.find({ user: check.id });
  } else {
    return res.status(400).send('Incomplete request');
  }

  if(tasks.length === 0) return res.status(404).send('No tasks available')

  let result = tasks.map(task => Task.format(task))
  return res.status(200).send(result);
});

taskController.put('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check = checkToken(token);
  if(!check) {
    return res.status(401).send('No access');
  }
  if(!req.body) {
    return res.status(400).send('The request body is missing.');
  }

  let result
  try {
    // TODO: make sure this is validated somehow. We don't want to save any crap
    // the FrontEnd sends here.
    result = await Task.updateOne({ _id: req.params.id }, { ...req.body.task });

  } catch (error) {
    return res.status(500).send('Error communicating with database');
  }
  return res.status(200).send(result);
});

taskController.delete('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check = checkToken(token);
  if(!check) {
    return res.status(401).send('No access');
  }

  let task
  if(req.params && req.params.id) {
    try {
      task = await Task.findByIdAndDelete(req.params.id);
      if(task === null) {
        return res.status(404).send('Item not found in database');
      }
    } catch (error) {
      return res.status(500).send('Error communicating with the database');
    }
  } else {
    return res.status(400).send('Incomplete request');
  }

  return res.status(200).json(Task.format(task));
});

module.exports = taskController;