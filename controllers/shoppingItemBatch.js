const shoppingItemBatchController = require('express').Router();
const ShoppingItemBatch = require('../models/shoppingItemBatch');
const { getTokenFrom, checkToken } = require('../helpers/authenticator');

shoppingItemBatchController.post('/', async (req, res) => {
  let token = getTokenFrom(req);
  let decodedToken
  
  if(token) { decodedToken = checkToken(token); }
  if(!decodedToken) {
    return res.status(401).send('No access');
  }

  if(req.body && req.body.batch) {
    const batch = req.body.batch;
    const batchName = batch.name;
    const items = batch.items;

    const newBatch = new ShoppingItemBatch({
      name: batchName,
      user: decodedToken.id,
      items: items ? items : [],
    });

    let savedBatch
    try {
      savedBatch = await newBatch.save();
    } catch (error) {
      return res.status(500).send('Error communicating with the database');
    }

    return res.status(200).json(savedBatch);
  } else {
    return res.status(400).send();
  }
});

shoppingItemBatchController.get('/', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }

  let items
  if(check.id) {
    items = await ShoppingItemBatch.find({ user: check.id });
  } else {
    return res.status(400).send('Incomplete request');
  }
  let result = items.map(item => ShoppingItemBatch.format(item));
  return res.status(200).send(result);
});

shoppingItemBatchController.get('/:id', async (req, res) => {

});

shoppingItemBatchController.delete('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }

  let item
  let result
  if(req.params && req.params.id) {
    try {
      item = await ShoppingItemBatch.findById(req.params.id);
      if(item === null) {
        return res.status(404).send('Item not found in database');
      }
      if(item.user === check.id) {
        result = await ShoppingItemBatch.findByIdAndDelete(req.params.id);
      } else {
        return res.status(401).send('No access');
      }
    } catch (error) {
      return res.status(500).send('Error communicating with the database');
    }
  } else {
    return res.status(400).send('Incomplete request');
  }

  return res.status(200).json(ShoppingItemBatch.format(result));
});

module.exports = shoppingItemBatchController;