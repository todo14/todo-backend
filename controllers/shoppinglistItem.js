const shoppinglistItemController = require('express').Router();
const ShoppinglistItem = require('../models/shoppinglistItem');
const User = require('../models/user');
const { getTokenFrom, checkToken } = require('../helpers/authenticator')
const logger = require('../utils/logger');
const { broadCastDataToAllClients } = require('./websocket');

shoppinglistItemController.post('/', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }

  let user;
  try {
    user = await User.findById(check.id);
    logger.info('Decoded token: ', check);
    logger.info('Found user ', user.id);
  } catch (error) {
    logger.error(error);
  }

  if(req.body && req.body.items) {
    const items = req.body.items;
    const newItems = items.map(i =>
      new ShoppinglistItem({
        ...i,
        done: false,
        user: user.id || user._id,
      })
    );
    
    logger.info('About to save shoppinglist items... ', newItems);

    let savedItems
    try {
      savedItems = await ShoppinglistItem.insertMany(newItems);

      logger.info('Adding saved items to Users list...');
      savedItems.forEach(i => {
        logger.info('Adding: ', i._id);
        user.shoppinglistItems = user.shoppinglistItems.concat(i._id);
      });

      logger.info('Trying to save user with updated shoppinglistItems...');
      await User.updateOne({ _id: user.id }, user);
      logger.info('User updated with new shoppinglistItems.');
    } catch (error) {
      return res.status(500).send('Error communicating with the database');
    }

    broadCastDataToAllClients(savedItems.map(i => ShoppinglistItem.format(i)));
    return res.status(200).json(savedItems.map(i => ShoppinglistItem.format(i)));
  } else {
    return res.status(400).send();
  }
});

shoppinglistItemController.delete('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }

  let item
  let result
  if(req.params && req.params.id) {
    try {
      item = await ShoppinglistItem.findById(req.params.id);
      if(item === null) {
        return res.status(404).send('Item not found in database');
      }

      logger.info('Checking if user is the owner of this shoppinglistItem...');
      if(item.user && item.user.toString() === check.id) {
        result = await ShoppinglistItem.findByIdAndDelete(req.params.id);
      } else {
        return res.status(401).send('No access');
      }
    } catch (error) {
      return res.status(500).send('Error communicating with the database');
    }
  } else {
    return res.status(400).send('Incomplete request');
  }

  broadCastDataToAllClients(JSON.stringify(ShoppinglistItem.format(result)));
  return res.status(200).json(ShoppinglistItem.format(result));
});

shoppinglistItemController.put('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }
  if (!req.params || !req.params.id) return res.status(400).send('Item ID missing');
  if(!req.body || !req.body.item) return res.status(400).send('New data missing from request');

  let item;
  let result;
  
  try {
    item = await ShoppinglistItem.findById(req.params.id);
    if (item === null) return res.status(400).send('Item not found');

    logger.info('Checking if user is the owner of this shoppinglistItem...');
    if (!item.user || item.user.toString() !== check.id) {
      return res.status(401).send('No access');
    }

    logger.info('Saving updated item...');
    result = await ShoppinglistItem.updateOne({ _id: req.params.id }, { ...req.body.item });
  } catch (error) {
    console.error(error);
    return res.status(500).send('Error communicating with the database');
  }
  return res.status(200).json(result);  
})

shoppinglistItemController.get('/', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }

  let items
  if(check.id) {
    items = await User.findById(check.id)
      .populate('shoppinglistItems');
  } else {
    return res.status(400).send('Incomplete request');
  }
  let result = items.shoppinglistItems.map(item => ShoppinglistItem.format(item));
  return res.status(200).send(result);
});

shoppinglistItemController.get('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check || !check.id) {
    return res.status(401).send('No access');
  }
  if(!req.params || !req.params.id) return res.status(400).send('Item ID missing');

  let item;
  let result;
  try {
    item = await ShoppinglistItem.findById(req.params.id);
    if (check.id !== item.user.toString()) return res.status(401).send('No access!');

    result = ShoppinglistItem.format(item);
    return res.status(200).json(item);
  } catch (error) {
    res.status(500).send('Error communicating with database');
  }

});

module.exports = shoppinglistItemController;