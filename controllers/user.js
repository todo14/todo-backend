const userController = require('express').Router();
const User = require('../models/user');
const bcrypt = require('bcrypt');
const { getTokenFrom, checkToken } = require('../helpers/authenticator')

userController.get('/', async(req, res) => {
  return res.status(401).send('No access');
});

userController.get('/:id', async (req, res) => {
  return res.status(401).send('No access');
});

userController.post('/', async (req, res) => {
  //TODO: check credentials

  if(req.body && req.body.password && req.body.username) {
    let body = req.body;
    const saltRounds = 10;

    if(body.password !== body.repassword) {
      return res.status(400).send('Passwords did not match');
    }
    const passwordHash = await bcrypt.hash(req.body.password, saltRounds);

    const user = new User({
      username: req.body.username,
      passwordHash: passwordHash
    });

    let savedUser

    try {
      savedUser = await user.save();
    } catch (error) {
      return res.status(500).send('Error, unable to save user to database');
    }

    return res.status(200).json(User.format(savedUser));

  } else {
    return res.status(400).send('Bad request');
  }

  
});

userController.delete('/:id', async(req, res) => {
  let token = getTokenFrom(req);
  let loggedUser
  if(token) { loggedUser = checkToken(token); }
  if(!loggedUser) {
    return res.status(401).send('No access');
  }

  const userToDeleteID = req.params.id
  if(userToDeleteID !== loggedUser.id) {
    return res.status(401).send()
  }

  try {
    await User.findByIdAndDelete(userToDeleteID);
  } catch(error) {
    return res.status(500).send('Error communicating with the database');
  }
  return res.status(200).send();
})

module.exports = userController;