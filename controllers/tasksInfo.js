const tasksInfoController = require('express').Router();
const authenticator = require('../helpers/authenticator');
const jwt = require('jsonwebtoken');

const getTokenFrom = (request) => {
  const authorization = request.get('authorization')
  if (authorization && authorization.toLowerCase().startsWith('bearer ')) {
    return authorization.substring(7)
  }
  return null
}

const checkToken = (token) => {
  if(token === null) {
    return false;
  }
  let decodedToken
  try {
    decodedToken = jwt.verify(token, process.env.SECRET);
  } catch (error) {
    console.log('JWT token invalid: ', error.message);
  } 
  if(!decodedToken) {
    return false;
  }
  return decodedToken;
}

tasksInfoController.post('/', async (req, res) => {

});

tasksInfoController.delete('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check = checkToken(token);
  if(!check || !check.id) {
    return res.status(401).send('No access');
  }
  let result;
  try {
    result = await tasksInfoController.findByIdAndDelete(req.params.id);
  } catch (error) {
    console.log('Error')
    return res.status(500).send('Error communicating with database');
  }
  return res.status(200).send(result);
});

tasksInfoController.put('/:id', async (req, res) => {

});

tasksInfoController.get('/:id', async (req, res) => {

});

tasksInfoController.get('/', async (req, res) => {
  return res.status(401).send('No access');
})