const storeController = require('express').Router();
const Store = require('../models/store');
const User = require('../models/user');
const logger = require('../utils/logger');

const { getTokenFrom, checkToken } = require('../helpers/authenticator');

storeController.post('/', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }

  let user;
  try {
    user = await User.findById(check.id);
    logger.info('Found user');
  } catch (error) {
    logger.error(error);
  }

  if(!req.body) return res.status(400).send();
  console.log('Request body: ', req.body);

  const newStore = new Store({
    ...req.body,
    user: user.id || user._id,
  });

  let savedStore;
  try {
    logger.info('Saving new Store...');
    savedStore = await newStore.save();
  } catch (error) {
    logger.error('Failed saving new Store', error);
    return res.status(500).send('Failed saving new Store to database');
  }

  return res.status(200).send(Store.format(savedStore));
});

storeController.put('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }
});

storeController.delete('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }
});

storeController.get('/', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }
  
  let stores;
  try {
    logger.info('Retrieving all Store documents...');
    stores = await Store.find({});
  } catch (error) {
    logger.error(error);
    return res.status(500).send('Error retrieving Store list');
  }

  const result = stores.map(store => Store.format(store));
  return res.status(200).send(result);
});

storeController.get('/:id', async (req, res) => {
  let token = getTokenFrom(req);
  let check
  if(token) { check = checkToken(token); }
  if(!check) {
    return res.status(401).send('No access');
  }
});

module.exports = storeController;