const loginController = require('express').Router();
const authenticator = require('../helpers/authenticator');
const jwt = require('jsonwebtoken');
const User = require('../models/user');


loginController.post('/', async (req, res) => {
  if(req.body && req.body.username) {
    const body = req.body
    let user
    try {
      user = await User.findOne({ username: body.username })
    } catch(e) {
      return res.status(500).send('Unable to fetch data from database');
    }

    const passwordMatch = user === null ?
      false :
      await authenticator.authenticate(body.password, user.passwordHash);

    if(!user) {
      return res.status(404).send('No user by this name');
    } 
    if(!passwordMatch) {
      return res.status(401).send('Invalid username or password');
    }

    const userForToken = {
      username: user.username,
      id: user._id
    };

    const token = jwt.sign(userForToken, process.env.SECRET, {expiresIn: "1h"});

    user = User.format(user);
    res.setHeader('Access-Control-Allow-Origin', '*');
    return res.status(200).send({ token, user });
  } else {
    return res.status(400).send();
  }
});

module.exports = loginController;