const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')
const api = supertest(app)

const User = require('../models/user')
const Task = require('../models/task')


const { validTasks } = require('../testdata/tasks')
const { validUser4, validLogin4 } = require('../testdata/users')

let token = ""
let user = ""
let validTasksWithUsersId = []
const taskUrl = "/api/task"
const userUrl = "/api/user"
const loginUrl = "/api/login"

beforeAll(async () => {
  await User.deleteOne({ username: validUser4.username })
  await Task.deleteMany({})
  const reg = await api
    .post(userUrl)
    .send(validUser4)

  user = reg.body.id

  validTasksWithUsersId = validTasks.map(t => {
    return {
      ...t,
      user: user
    }
  })

  const res = await api
    .post(loginUrl)
    .send(validLogin4)

  token = res.body.token
})

afterAll(async () => {
  await User.deleteOne({ username: validUser4.username })
  mongoose.connection.close()
})

describe('When user is not authenticated', () => {
  it('User can not add tasks and will get appropriate response', async () => {
    const res = await api
      .post(taskUrl)
      .expect(401)
  })

  it('User is not allowed to fetch tasks', async () => {
    const res = await api
      .get(taskUrl)
      .expect(401)
  })

  it('User is not allowed to delete tasks', async () => {
    const res = await api
      .del(taskUrl + '/1')
      .expect(401)
  })

  it('User is not allowed to update tasks', async () => {
    const res = await api
      .put(taskUrl + '/1')
      .expect(401)
  })
})

describe('When user is authenticated', () => {
  describe('When user has no tasks in database', () => {
    it('User will receive a 404 message and no tasks when fetching', async () => {
      const res = await api
        .get(taskUrl)
        .set('Authorization', `Bearer ${token}`)
        .expect(404)
    })

    it('User can add tasks and response contains saved task', async () => {
      const res = await api
        .post(taskUrl)
        .set('Authorization', `Bearer ${token}`)
        .send({ task: validTasksWithUsersId[0] })
        .expect(200)
        .expect('Content-Type', /application\/json/)

      const taskID = res.body.id
      await Task.findByIdAndRemove(taskID)

      expect(res.body.description).toBe(validTasks[0].description)
      expect(res.body.label).toBe(validTasks[0].label)
    })
  })

  describe('When user has own tasks in database', () => {
    it('User can update existing own tasks', async () => {
      const task = new Task(validTasksWithUsersId[0])
      const savedTask = await task.save()

      const res = await api
        .put(taskUrl + `/${savedTask.id}`)
        .set('Authorization', `Bearer ${token}`)
        .send({ task: {
          ...validTasksWithUsersId[0],
          label: "New label"
        }})
        .expect(200)
        .expect('Content-Type', /application\/json/)

      await Task.findByIdAndRemove(savedTask.id)

      expect(res.body.label).toBe('New label')
    })

    it('User can delete existing own tasks', async () => {
      const task = new Task(validTasksWithUsersId[0])
      const savedTask = await task.save()

      const res = await api
        .del(taskUrl + `/${savedTask.id}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
    })

    it('User can fetch own existing tasks', async () => {
      await Task.insertMany(validTasksWithUsersId)

      const res = await api
        .get(taskUrl)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
        .expect('Content-Type', /application\/json/)

      expect(res.body.length).toBe(3)

      await Task.deleteMany({})
    })
  })
})