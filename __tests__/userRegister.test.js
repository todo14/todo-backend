const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')
const api = supertest(app)

const userUrl = "/api/user"
const loginUrl = "/api/login"

const User = require('../models/user')

const {
  validUser3,
  validLogin3,
  userWithMismatchPasswords,
  userWithoutUsername,
} = require('../testdata/users');

beforeEach(async () => {
  await User.deleteOne({ username: validUser3.username })
})

afterEach(async () => {
  await User.deleteOne({ username: validUser3.username })
})

afterAll( () => {
  mongoose.connection.close()
})

describe('Register User', () => {
  it('User should be able to register with valid data', async (done) => {
    const res = await api
      .post(userUrl)
      .send(validUser3)
      .expect(200)
      .expect('Content-Type', /application\/json/)

    done()
  });

  it('User cant register without username', async (done) => {
    const res = await api
      .post(userUrl)
      .send(userWithoutUsername)
      .expect(400)

    done();
  });

  it('User cant register with mismatching passwords', async (done) => {
    const res = await api
      .post(userUrl)
      .send(userWithMismatchPasswords)
      .expect(400)

    done()
  });

  it('User cant register without password', async (done) => {
    const res = await api
      .post(userUrl)
      .send({username: "username"})
      .expect(400)

    done()
  });

  describe('When user is authenticated', () => {
    it('User can delete his own account', async () => {
      const register = await api
        .post(userUrl)
        .send(validUser3)

      const login = await api
        .post(loginUrl)
        .send(validLogin3)

      const userID = register.body.id
      const token = login.body.token

      await api
        .del(userUrl + `/${userID}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(200)
    })

    it('User can not delete other accounts', async () => {
      await User.deleteOne({ username: "anotherUser"})
      const register = await api
        .post(userUrl)
        .send(validUser3)

      let anotherUser = new User({
        ...validUser3,
        username: "anotherUser"
      })
      let savedOtherUser = anotherUser.save()

      const login = await api
        .post(loginUrl)
        .send(validLogin3)


      const res = await api
        .del(`${userUrl}/${savedOtherUser.id}`)
        .set('Authorization', `Bearer ${login.body.token}`)
        .expect(401)

      await User.deleteOne({ username: "anotherUser"})
    })
  })

  describe('When user is not authenticated', () => {
    it('User can not fetch all users', async () => {
      const res = await api
        .get(userUrl)
        .expect(401)
    })

    it('User can not fetch a user by id', async () => {
      const res = await api
        .get(`${userUrl}/1`)
        .expect(401)
    })
  })
})