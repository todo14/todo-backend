const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')
const api = supertest(app)

const User = require('../models/user')

const loginUrl = "/api/login"
const userUrl = "/api/user"

const {
  validLogin1,
  validUser1,
  invalidPasswordLogin,
  invalidUsernameLogin
} = require('../testdata/users');



afterAll(async () => {
  await User.deleteOne({ username: validUser1.username })
  mongoose.connection.close()
})

beforeAll(async () => {
  await User.deleteOne({ username: validUser1.username })
  const register = await api
    .post(userUrl)
    .send(validUser1)
})

describe('LOGIN - When there is at least one user in database', () => {
  it('User can log in with correct credentials', async (done) => {
    await api
      .post(loginUrl)
      .send(validLogin1)
      .expect(200)

    done();
  });

  it('User can not be logged in with incorrect password', async (done) => {
    await api
      .post(loginUrl)
      .send(invalidPasswordLogin)
      .expect(401)

    done();
  })

  it('User can not login with nonexisting username', async () => {
    await api
      .post(loginUrl)
      .send(invalidUsernameLogin)
      .expect(404)
  })
});