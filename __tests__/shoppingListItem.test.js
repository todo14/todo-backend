const supertest = require('supertest')
const app = require('../app')
const mongoose = require('mongoose')
const api = supertest(app)

const ShoppinglistItem = require('../models/shoppinglistItem')
const User = require('../models/user')


const { validItems } = require('../testdata/shoppinglistitems')
const { validUser2, validLogin2 } = require('../testdata/users')

let token = ""
let user = ""
let url = "/api/shoppinglist"

beforeAll(async () => {
  await User.deleteOne({ username: validUser2.username })
  await ShoppinglistItem.deleteMany({})

  const reg = await api
    .post('/api/user')
    .send(validUser2)

  user = reg.body.id
  
  const res = await api
    .post('/api/login')
    .send(validLogin2)

    token = res.body.token

    await ShoppinglistItem.insertMany(validItems.map(i => {
      return {
        ...i,
        user: user
      }
    }))
})



afterAll(async () => {
  await User.deleteOne({ username: validUser2.username })
  await ShoppinglistItem.deleteMany({})
  mongoose.connection.close()
})

describe('SHOPPINGLIST - When user has added at least two items', () => {
  it('User can fetch all own items from api and gets proper status', async () => {
    const res = await api
      .get(url)
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .expect('Content-Type', /application\/json/)

    expect(res.body.length).toBe(2)
  })
})

describe('SHOPPINGLIST - When two users have both added two items', () => {
  it('User can fetch own items and no more', async () => {
    await ShoppinglistItem.insertMany(validItems)

    const res = await api
      .get(url)
      .set('Authorization', `Bearer ${token}`)
      .expect(200)
      .expect('Content-Type', /application\/json/)

    expect(res.body.length).toBe(2)
  })

  it('Logged in user can add items', async () => {
    const res = await api
      .post(url)
      .set('Authorization', `Bearer ${token}`)
      .send({ item: validItems[0] })
      .expect(200)
      .expect('Content-Type', /application\/json/)

    expect(res.body.name).toBe(validItems[0].name)
  })

  it('Logged in user can not add a task with empty request body', async () => {
    const res = await api
    .post(url)
    .set('Authorization', `Bearer ${token}`)
    .send({})
    .expect(400)
  })


})

describe('SHOPPINGLIST - When user is not logged in', () => {
  it('Unauthorized users can not fetch any items and receive 401 status', async () => {
    const res = await api
      .get(url)
      .expect(401)

    await api
      .get(url + '/1')
      .expect(401)

    expect(Object.entries(res.body).length).toBe(0)
  })

  it('Unauthed users can not add item and receive 401 status', async () => {
    const res = await api
      .post(url)
      .send(validItems[0])
      .expect(401)
  })

  it('Unauthed users can not delete items and receive 401 status', async () => {
    const res = await api
      .del(url + '/1')
      .expect(401)
  })

  it('Unauthed users can not update items and receive 401 status', async () => {
    const res = await api
      .put(url + '/1')
      .expect(401)
  })
})