const mongoose = require('mongoose');

const tasksInfoSchema = new mongoose.Schema({
  avgTime: Number,
  amountDone: Number,
  totalTime: Number,
});

tasksInfoSchema.statics.format = (tasksInfo) => {
  return {
    avgTime: tasksInfo.avgTime,
    amountDone: tasksInfo.amountDone,
    totalTime: tasksInfo.totalTime,
  };
};

const TasksInfo = mongoose.model('TasksInfo', tasksInfoSchema);

module.exports = TasksInfo;