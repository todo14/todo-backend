const mongoose = require('mongoose');

const storeSchema = new mongoose.Schema({
  location: String,
  name: String,
  postalCode: Number,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  created: { type: Date, default: Date.now },
});

storeSchema.statics.format = (store) => {
  return {
    id: store._id,
    location: store.location,
    name: store.name,
    postalCode: store.postalCode,
    user: store.user,
    created: store.created,
  };
};

const Store = mongoose.model('Store', storeSchema);

module.exports = Store;