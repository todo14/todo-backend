const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    priority: Number,
    label: String,
    description: String,
    done: Boolean,
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    created: { type: Date, default: Date.now },
});

taskSchema.statics.format = (task) => {
  return {
    id: task._id,
    priority: task.priority,
    label: task.label,
    description: task.description,
    done: task.done,
    user: task.user,
    created: task.created,
  };
};

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;