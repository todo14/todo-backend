const mongoose = require('mongoose');

const shoppinglistItemSchema = new mongoose.Schema({
  name: String,
  done: Boolean,
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  store: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Store',
  },
});

shoppinglistItemSchema.statics.format = (shoppinglistItem) => {
  return {
    id: shoppinglistItem._id,
    name: shoppinglistItem.name,
    done: shoppinglistItem.done,
    store: shoppinglistItem.store,
    user: shoppinglistItem.user,
  };
};

const ShoppinglistItem = mongoose.model('ShoppinglistItem', shoppinglistItemSchema);

module.exports = ShoppinglistItem;