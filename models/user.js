const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    require: true
  },
  passwordHash: String,
  tasks: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Task',
    },
  ],
  shoppinglistItems: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'ShoppinglistItem',
    },
  ],
});

userSchema.statics.format = (user) => {
  return {
    id: user._id,
    username: user.username,
    tasks: user.tasks,
    shoppinglistItems: user.shoppinglistItems,
  };
};

const User = mongoose.model('User', userSchema);

module.exports = User;