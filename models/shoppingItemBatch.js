const mongoose = require('mongoose');
const ShoppinglistItem = require('./shoppinglistItem');

const shoppingItemBatchSchema = new mongoose.Schema({
  user: String,
  name: String,
  items: Array,
});

shoppingItemBatchSchema.statics.format = (shoppingItemBatch) => {
  return {
    id: shoppingItemBatch._id,
    user: shoppingItemBatch.user,
    name: shoppingItemBatch.name,
    items: shoppingItemBatch.items,
  };
};

const ShoppingItemBatch = mongoose.model('ShoppingItemBatch', shoppingItemBatchSchema);

module.exports = ShoppingItemBatch;