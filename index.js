const app = require('./app');
const ws = require('ws');
const config = require('./utils/config')

const PORT = config.PORT || 3030;
const server = app.listen(PORT, () => {
  console.log('Server running in', PORT);
});

const webSocketServer = new ws.Server({ noServer: true });
webSocketServer.on('connection', socket => {
  socket.on('message', message => {
    console.log('Message received: ', message);
  });
});

server.on('upgrade', (req, socket, head) => {
  console.log('onUpgrade');
  webSocketServer.handleUpgrade(req, socket, head, socket => {
    webSocketServer.emit('connection', socket, req);
  });
});