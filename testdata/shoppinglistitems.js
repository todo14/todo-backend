const validItems = [
  {
    name: "maito",
    done: false,
    store: "prisma",
    user: "set_user_id_here"
  },
  {
    name: "kurkku",
    done: false,
    store: "lidl",
    user: "set_user_id_here"
  }
]

module.exports = {
  validItems
}