const userWithoutUsername = {
  password: "testpassword",
  repassword: "testpassword"
}

const validUser1 = {
  username: "testuser",
  password: "testpassword",
  repassword: "testpassword"
}

const validUser2 = {
  username: "testuser2",
  password: "testpassword2",
  repassword: "testpassword2"
}

const validUser3 = {
  username: "testuser3",
  password: "testpassword3",
  repassword: "testpassword3"
}

const validUser4 = {
  username: "testuser4",
  password: "testpassword4",
  repassword: "testpassword4"
}

const userWithMismatchPasswords = {
  ...validUser1,
  repassword: "wrongpassword"
}

const validLogin1 = {
  username: "testuser",
  password: "testpassword"
}

const validLogin2 = {
  username: "testuser2",
  password: "testpassword2"
}

const validLogin3 = {
  username: "testuser3",
  password: "testpassword3"
}

const validLogin4 = {
  username: "testuser4",
  password: "testpassword4"
}

const invalidPasswordLogin = {
  username: "testuser",
  password: "wrongpassword"
}

const invalidUsernameLogin = {
  username: "nonexisting",
  password: "testpassword"
}

module.exports = {
  userWithMismatchPasswords,
  userWithoutUsername,
  validLogin1,
  invalidPasswordLogin,
  invalidUsernameLogin,
  validUser1,
  validUser2,
  validUser3,
  validUser4,
  validLogin2,
  validLogin3,
  validLogin4
}