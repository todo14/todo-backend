const validTasks = [
  {
    priority: 1,
    label: "First label",
    description: "First description",
    done: false,
    user: "user_id_here"
  },
  {
    priority: 2,
    label: "Second label",
    description: "Second description",
    done: false,
    user: "user_id_here"
  },
  {
    priority: 5,
    label: "Third label",
    description: "",
    done: false,
    user: "user_id_here"
  }
]

module.exports = {
  validTasks
}